from pyfias.table.xml import XMLFormat
from pyfias.loader import Loader
from pyfias.data.directory import DirectoryDataList
from examples.models import tables

if __name__ == "__main__":

    # Получаем список файлов
    datalist = DirectoryDataList(source = 'path')

    # Заполняем выбранные таблички
    for data in datalist.tabels(XMLFormat):
        if tables.get(data):
            loader = Loader()
            loader.load()
        else:
            print('Not found data table in datalist')
