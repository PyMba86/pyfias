from examples.models.history import History
from pyfias.models.fields import *


class Room(History):
    """
    Классификатор помещений
    """
    houseguid = IntegerField()
    roomguid = UUIDField()
    roomid = UUIDField()

