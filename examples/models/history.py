from pyfias.models import Model
from pyfias.models.fields import *


class History(Model):
    """
    Абстрактная модель, которая содержит поля,
    для связывания с исторической записью
    """
    previd = UUIDField()
    nextid = UUIDField()
