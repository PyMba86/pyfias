from pyfias.table import Table, TableIterator


class XMLIterator(TableIterator):

    def format_row(self, row):
        pass


class XMLTable(Table):
    prefix = 'as_'
    pattern = r'(?P<deleted>del_)?(?P<name>[a-z]+)_(?P<date>\d+)_(?P<uuid>[a-z0-9-]{36}).xml'
