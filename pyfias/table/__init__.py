class TableIterator(object):
    """
    Итератор для прохода по файлу
    """

    def __init__(self, model):
        self.model = model

    def format_row(self, row):
        raise NotImplementedError()


class Table(object):
    """
    Формат данных, для получения данных
    """
    pattern = None
