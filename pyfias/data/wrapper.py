import abc
import datetime
import os
import shutil


class DataWrapper(object):
    """
    Обертка над данными, для работы с файлами
    """
    source = None

    def __init__(self, source, **kwargs):
        pass

    @abc.abstractmethod
    def get_date_info(self, name: str):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_file_list(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def open(self, name: str):
        raise NotImplementedError()


class DirectoryWrapper(DataWrapper):
    is_temporary = False

    def __init__(self, source, is_temporary=False, **kwargs):
        super(DirectoryWrapper, self).__init__(source=source, **kwargs)
        self.is_temporary = is_temporary
        self.source = os.path.abspath(source)

    def get_date_info(self, name: str):
        state = os.stat(os.path.join(self.source, name))
        return datetime.datetime.fromtimestamp(state.st_atime)

    def get_file_list(self):
        return [file for file in os.listdir(self.source) if
                (not file.startswith('.') and os.path.isfile((os.path.join(self.source, file))))]

    def get_full_path(self, filename):
        return os.path.join(self.source, filename)

    def open(self, name: str):
        return open(self.get_full_path(name), 'rb')

    def __del__(self):
        if self.is_temporary:
            shutil.rmtree(self.source, ignore_errors=True)
