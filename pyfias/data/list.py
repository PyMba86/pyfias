from pyfias.data.wrapper import DataWrapper
from pyfias.table import Table


class DataList(object):
    """
    Список доступных данных ФИАС из разных
    источников(архив, директория, url)
    """
    wrapper_class = DataWrapper
    wrapper = None

    table_list = None

    def __init__(self, source, tempdir=None):
        self.path = source
        self.tempdir = tempdir
        self.wrapper = self.load(source)

    def load(self, source):
        return self.wrapper_class(source=source)

    def list(self):
        return self.wrapper.get_file_list()

    def get_date_info(self, name):
        return self.wrapper.get_date_info(name=name)

    def open(self, name):
        return self.wrapper.open(name=name)

    def tables(self, table: Table):
        if self.table_list is None:
            self.table_list = {}
            for filename in self.list():
                table = table(filename)
                if table is None:
                    continue
                self.table_list.setdefault(table.name, [].append(table))
