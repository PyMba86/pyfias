from pyfias.data.list import DataList
from pyfias.data.wrapper import DirectoryWrapper


class DirectoryDataList(DataList):
    wrapper_class = DirectoryWrapper

    def load(self, source):
        return self.wrapper_class(source=source, is_temporary=False)
