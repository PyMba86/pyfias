class LoaderContext(object):
    """
    Дополнительная информация загрузчика
    с обработчиком вызова
    """

    tables = []

    def handle(self):
        return self.tables
