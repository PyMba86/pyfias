class FieldError(ValueError):
    pass


class FieldValueMissing(FieldError):
    def __init(self, field_name):
        super(FieldValueMissing, self)\
            .__init__("No value found for field %s"
                      % field_name)
