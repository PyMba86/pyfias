import re
import abc


class Validator(object):
    """
    Абстрактный класс кастомного валидатора
    для атрибута модели
    """

    @abc.abstractmethod
    def validate(self, value: str) -> bool:
        pass


class GenericRegexValidator(Validator):
    """
    Валидатор аттрибута по регулярному выражению
    """

    def validate(self, value: str) -> bool:
        return bool(re.match(self.regex, value))


class DateValidator(Validator):
    """
    Валидатор аттрибута по дате
    """

    def validate(self, value: str) -> bool:
        pass
