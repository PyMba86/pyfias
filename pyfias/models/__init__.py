import copy

from pyfias.models.exceptions import FieldValueMissing
from pyfias.models.fields import Field

__all__ = ["Model", "fields"]


class Model(object):

    def __init__(self, data):
        self.cls = self.__class__
        self.attrs = self.get_fields()
        self.errors = []
        self.construct_obj_from_data(data)

    def is_valid(self):
        return len(self.errors) == 0

    def construct_obj_from_data(self, data):
        for position, (field_name, field) in enumerate(self.attrs):
            field.position = position
            value = data.pop(0)
            try:
                self.set_field_value(field_name, field, value)
            except Exception as e:
                    raise e

    @staticmethod
    def set_values(values_dict, fields_name, values):
        if isinstance(fields_name, list):
            for field_name in fields_name:
                values_dict[field_name] = values
        else:
            values_dict[fields_name] = values

    @classmethod
    def get_fields(cls):
        all_cls_dict = {}
        all_cls_dict.update(cls.__dict__)
        for claaz in cls.__bases__:
            all_cls_dict.update(claaz.__dict__)

        # Добавляем аттрибуты, которых нет
        # в результирующем классе
        attributes = [(attr, copy.copy(all_cls_dict[attr]))
                      for attr in all_cls_dict
                      if isinstance(all_cls_dict[attr], Field)]

        for fieldname, field in attributes:
            field.fieldname = fieldname

        sorted_field = sorted(attributes, key=lambda attrs: attrs[1].position)
        return sorted_field

    @classmethod
    def get_data_fields(cls):
        return [fieldname for (fieldname, field) in cls.get_fields()
                if fieldname not in getattr(cls, "_exclude_data_fields", [])]

    def as_dict(self):
        return dict((field, getattr(self, field))
                    for field in self.get_data_fields())

    def set_field_value(self, field_name, field, value):
        try:
            self.__dict__[field_name] = field.get_prep_value(value)
        except IndexError:
            raise FieldValueMissing(field_name)

    def get_field_value(self, field_name):
        return self.__dict__[field_name]
