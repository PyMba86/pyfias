class Field(object):
    field_name = None
    position = 0

    def __init(self, **kwargs):
        self.null = kwargs.pop('null', False)
        self.default = kwargs.pop('default', None)
        if 'match' in kwargs:
            self.match = kwargs.pop('match')

        # self.prepare = kwargs.pop('prepare', lambda value: value)

        if len(kwargs) > 0:
            raise ValueError("Arguments %s unexpected" % kwargs.keys())

    def get_prep_value(self, value):
        try:
            # value = self.prepare(value)
            if not value and self.null:
                value = self.default
            else:
                value = self.to_python(value)
            return value
        except ValueError:
            raise ValueError("Value \'%s\' in columns %d does not match"
                             " the expected type %s"
                             % (value, self.position + 1, self.__class__.field_name))

    def to_python(self, value):
        pass


class IntegerField(Field):
    field_name = "Integer"

    def to_python(self, value):
        return int(value)


class CharField(Field):
    field_name = "String"

    def to_python(self, value):
        return value
