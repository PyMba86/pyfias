# Загрузчик получает на вход модельку и
# максимальное число для загрузки в память
# При вызове load - происходит
# При вызове create -
# происходит возврат заполненных моделек
from pyfias.context import LoaderContext


class Loader(object):

    def filter(self):
        # TODO Добавить фильтры
        return None

    def context(self, context: LoaderContext):
        context.handle()
        return self

    def load(self):
        return True

    def create(self):
        return None




