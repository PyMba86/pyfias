from unittest import TestCase
from pyfias.models import Model
from pyfias.models.fields import *


class ExampleModel(Model):
    name = CharField()
    age = IntegerField()


class TestModel(TestCase):

    def test_basic(self):
        test_data = ["Anna", 10]
        model = ExampleModel(test_data)
        self.assertEquals(model.name, "Anna")
        self.assertEquals(model.age, 10)
