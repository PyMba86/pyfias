import datetime
import tempfile
from unittest import TestCase
from unittest.mock import patch

from pyfias.data.wrapper import (
    DataWrapper,
    DirectoryWrapper,
)


class TestDataWrapper(TestCase):

    def setUp(self):
        self.wrapper = DataWrapper(None)

    def test_getting_date_info(self):
        self.assertRaises(NotImplementedError,
                          self.wrapper.get_date_info,
                          name=None)

    def test_getting_file_list(self):
        self.assertRaises(NotImplementedError,
                          self.wrapper.get_file_list)

    def test_opening_file(self):
        self.assertRaises(NotImplementedError,
                          self.wrapper.open,
                          name=None)


class TestDirectoryWrapper(TestCase):

    def setUp(self):
        self.wrapper = DirectoryWrapper("directory")

    def test_getting_file_list(self):
        with patch('os.listdir') as mocked_listdir, \
                patch('os.path.isfile') as mocked_isfile:
            mocked_listdir.return_value = ['file1', 'file2', 'file3']
            mocked_isfile.return_value = True
            list = self.wrapper.get_file_list()
            self.assertEqual(list, ['file1', 'file2', 'file3'])

    def test_getting_date_info(self):
        os_stat_mock = patch('os.stat')
        os_stat_mock.start()
        date_info = self.wrapper.get_date_info('file')
        self.assertIsInstance(date_info, datetime.date)
        os_stat_mock.stop()

    def test_opening_file(self):
        filename = 'file'
        # TODO Добавить тест на открытие файла


class TestTemporaryDirectoryWrapper(TestCase):

    def setUp(self):
        temp = tempfile.mktemp()
        self.wrapper = DirectoryWrapper(temp, is_temporary=True)

    def test_deleting_temporary_data(self):
        path = self.wrapper.source
        # TODO Добавить тест на удаление файла
